#! /bin/sh
# Mes --- Maxwell Equations of Software
# Copyright © 2017,2018 Jan Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of Mes.
#
# Mes is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Mes is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mes.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" = "--version" ]; then
    echo -n "mes-seed (Mes) "
    cat "${0%/*}"/VERSION
    exit 0
fi

set -ex

VERSION=git
MES_PREFIX=${MES_PREFIX-../mes}
MES=guile
prefix=${prefix-}
moduledir=${moduledir-${prefix}/share/mes/module}
arch=${arch-x86-mes}
MESC_FLAGS="-v"

if [ "$arch" == "x86_64-mes" ]; then
    MESC_FLAGS="$MESC_FLAGS -m 64"
fi

export MES
export MES_PREFIX
export VERSION
export prefix
export moduledir

# crt1
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -c -E -I $MES_PREFIX/include -o crt1.E $MES_PREFIX/lib/linux/$arch/crt1.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/crt1.S crt1.E
rm crt1.E

# libc-mes
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -c -E -I $MES_PREFIX/include -I $MES_PREFIX/lib -o libc.E $MES_PREFIX/lib/libc.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/libc.S libc.E
rm libc.E

# libc-mes+tcc
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -c -E -I $MES_PREFIX/include -I $MES_PREFIX/lib -o libgetopt.E $MES_PREFIX/lib/libgetopt.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/libgetopt.S libgetopt.E
rm libgetopt.E

# libc-mes+tcc
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -c -E -I $MES_PREFIX/include -I $MES_PREFIX/lib -o libc+tcc.E $MES_PREFIX/lib/libc+tcc.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/libc+tcc.S libc+tcc.E
rm libc+tcc.E

# libc-mes+gnu
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -c -E -I $MES_PREFIX/include -I $MES_PREFIX/lib -o libc+gnu.E $MES_PREFIX/lib/libc+gnu.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/libc+gnu.S libc+gnu.E
rm libc+gnu.E

# mes
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/gc.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/lib.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/math.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/mes.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/posix.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/reader.c
$MES_PREFIX/build-aux/mes-snarf.scm --mes $MES_PREFIX/src/vector.c
$MES_PREFIX/pre-inst-env mescc\
    -v\
    -E\
    -D "VERSION=\"$VERSION\""\
    -D "MODULEDIR=\"$moduledir\""\
    -D "PREFIX=\"$prefix\""\
    -I $MES_PREFIX/include\
    -I $MES_PREFIX/src\
    -o mes.E\
    $MES_PREFIX/src/mes.c
$MES_PREFIX/pre-inst-env mescc $MESC_FLAGS -S -o $arch/mes.S mes.E
rm mes.E
